export default {
  pending: 'En attente',
  'to be verified': 'À vérifier',
  paid: 'Payée',
  created: 'Créée',
  ready: 'Prête',
  completed: 'Complète',
  cancelled: 'Annulée',
};
