export const sqlDateTimeFormat = 'yyyy-MM-dd HH:mm:ss';
export const sqlDateFormat = 'yyyy-MM-dd';
export const frenchDateTimeFormat = 'dd.MM.yyyy HH:mm';
