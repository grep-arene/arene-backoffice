import APIService from '@/service/APIService';

class LanguageService extends APIService {
  constructor() {
    super('/languages');
  }
}

export default new LanguageService();
