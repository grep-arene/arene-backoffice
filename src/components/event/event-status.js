export default Object.freeze({
  paid: 'paid',
  unpaid: 'unpaid',
  cancelled: 'cancelled',
});
