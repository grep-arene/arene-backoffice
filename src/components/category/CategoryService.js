import APIService from '@/service/APIService';

class CategoryService extends APIService {
  constructor() {
    super('/categories');
  }
}

export default new CategoryService();
