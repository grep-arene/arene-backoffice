import http from '@/http/token-http';

export default class LogoutService {
  static logout() {
    return http.post('/logout')
      .then((res) => res.data);
  }
}
