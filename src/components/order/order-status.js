export default Object.freeze({
  created: 'created',
  ready: 'ready',
  completed: 'completed',
  cancelled: 'cancelled',
});
