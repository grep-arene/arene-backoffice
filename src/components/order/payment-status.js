export default Object.freeze({
  pending: 'pending',
  toBeVerified: 'to be verified',
  paid: 'paid',
});
