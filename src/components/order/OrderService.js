import APIService from '@/service/APIService';

class OrderService extends APIService {
  constructor() {
    super('/orders');
  }
}

export default new OrderService();
