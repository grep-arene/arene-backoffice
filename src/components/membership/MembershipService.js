import APIService from '@/service/APIService';

class MembershipService extends APIService {
  constructor() {
    super('/memberships');
  }
}

export default new MembershipService();
