module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-vars': 'warn',
    'vue/no-unused-components': 'warn',
    'no-shadow': 'off',
    'linebreak-style': 'off',
    'spaced-comment': 'off',
    'import/extensions': 'warn',
    'no-throw-literal': 'off',
    'dot-notation': 'warn',
    'no-trailing-spaces': 'warn',
    'max-len': ['off', {
      code: 100,
      ignoreUrls: true,
    }],
  },
};
